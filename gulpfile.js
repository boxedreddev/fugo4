var elixir = require('laravel-elixir');
var theme = 'fugo4';
var domain = 'http://'+theme+'.local/';
var themeDir = 'wp-content/themes/'+theme+'/';
var themelibDir = 'wp-content/themes/'+theme+'/library/';

elixir.config.assetsPath = themelibDir
elixir.config.publicPath = themelibDir
elixir.config.css.outputFolder = 'css';
elixir.config.css.sass.folder = 'scss';

elixir(function(mix) {
	mix.sass('style.scss');
});

elixir(function(mix) {
	var frontendScripts = [
		'libs/particles.min.js',
		'libs/app.js',
		'libs/owl.carousel.min.js',
		'libs/ScrollMagic.min.js',
		'libs/greensock/TweenMax.min.js',
		'libs/scrollmagic_plugins/animation.gsap.min.js',
		'libs/scrollmagic_plugins/debug.addIndicators.min.js',
		'libs/*.js',
		'scripts.js'
	];

  mix.scripts(frontendScripts, themelibDir+'js/scripts.min.js');
});





elixir(function(mix) {
	mix.browserSync({
		open: false,
		proxy: domain,
		injectChanges: true,
		files: [
			themeDir+'/**/*.php',
			themelibDir+'/css/**/*.css',
			themelibDir+'/scss/sass/**/*.scss',
			themelibDir+'/js/**/*.js'
		]
	});
});
