<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

require_once(ABSPATH . 'environments.php');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $db_config[$env]['database']);

/** MySQL database username */
define('DB_USER', $db_config[$env]['username']);

/** MySQL database password */
define('DB_PASSWORD', $db_config[$env]['password']);

/** MySQL hostname */
define('DB_HOST', $db_config[$env]['host']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eeBsh9GJ>`;r|h]]^P~y2zp*);48(7FKUI[zputh%H&n=zK_Go[|]3LDn2E@Jd{e');
define('SECURE_AUTH_KEY',  'I7ha<z!Y^%1_tBoMdD,` Qt}1I4sB5L17[CsY7vy-zP]LZYhB2xAe~$||hL9Dw!a');
define('LOGGED_IN_KEY',    ',$Q|^2 =wPe+-ygBCtt*rPv_g|+Sk`g#_7|msaQ8,v6T-pvjF8Ii=rOhRE1pJ+O#');
define('NONCE_KEY',        '/~n#+/TzC5tzc-+iEWY{a({$-$$Rvt%0o7GG6l6r3!YYjYdkAx`}Pdn?wy9PVV|p');
define('AUTH_SALT',        'j,R*p$&)PJa,=*tx-;J{=veY|$c/-Hd^A4HP/rkCys%Y.9Q;^.mpb.#WScf6+c<R');
define('SECURE_AUTH_SALT', '1<YmUV[a_iq5zHp,jU8]jKOVtk~2QaYp%Lg086+-~b<&?B~r~K+B[If`8J0W-lgx');
define('LOGGED_IN_SALT',   '#Z0YO/k0cEUGcJb&~Q=$#XB;/wq8l(NOL)p2>.~u|4>4g xQT)Hrqw3W$f?RwOBi');
define('NONCE_SALT',       'j%:GeyyQ1gh?HYRoG]R{ZB(DB!Ga5GL8--BJtiR)vXC$KD|^_!}Jo%=WY.n=acMb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_MEMORY_LIMIT', '64M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
