<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>

			<!--favicons-->
			<link rel="apple-touch-icon" sizes="57x57" href="<? echo image('icons/apple-icon-57x57.png'); ?>">
			<link rel="apple-touch-icon" sizes="60x60" href="<? echo image('icons/apple-icon-60x60.png'); ?>">
			<link rel="apple-touch-icon" sizes="72x72" href="<? echo image('icons/apple-icon-72x72.png'); ?>">
			<link rel="apple-touch-icon" sizes="76x76" href="<? echo image('icons/apple-icon-76x76.png'); ?>">
			<link rel="apple-touch-icon" sizes="114x114" href="<? echo image('icons/apple-icon-114x114.png'); ?>">
			<link rel="apple-touch-icon" sizes="120x120" href="<? echo image('icons/apple-icon-120x120.png'); ?>">
			<link rel="apple-touch-icon" sizes="144x144" href="<? echo image('icons/apple-icon-144x144.png'); ?>">
			<link rel="apple-touch-icon" sizes="152x152" href="<? echo image('icons/apple-icon-152x152.png'); ?>">
			<link rel="apple-touch-icon" sizes="180x180" href="<? echo image('icons/apple-icon-180x180.png'); ?>">
			<link rel="icon" type="image/png" sizes="192x192"  href="<? echo image('icons/android-icon-192x192.png'); ?>">
			<link rel="icon" type="image/png" sizes="32x32" href="<? echo image('icons/favicon-32x32.png'); ?>">
			<link rel="icon" type="image/png" sizes="96x96" href="<? echo image('icons/favicon-96x96.png'); ?>">
			<link rel="icon" type="image/png" sizes="16x16" href="<? echo image('icons/favicon-16x16.png'); ?>">
			<meta name="msapplication-TileColor" content="#52A3DB">
			<meta name="msapplication-TileImage" content="<? echo image('icons/ms-icon-144x144.png'); ?>">
			<meta name="theme-color" content="#1F4B70">


	  <link href="<?php echo get_template_directory_uri(); ?>/library/css/all.min.css" rel="stylesheet"> <!--load all styles -->
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<!-- Global site tag (gtag.js) - Google Analytics -->

			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126581326-1"></script>

			<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

					ga('create', 'UA-126581326-1', 'auto');
					ga('require', 'linkid');
					ga('send', 'pageview');
			</script>

		<? if( have_rows('tracking_codes',  'options') ): ?>

			<script>
				document.addEventListener( 'wpcf7mailsent', function( event ) {

				<? while ( have_rows('tracking_codes','options') ) : the_row(); ?>

		        	if(event.detail.contactFormId == '<?= the_sub_field('form_id', 'options') ?>') {
				    	console.log('<?= the_sub_field('event_name', 'options') ?>-tracking-sent');
				   		//ga('send', 'event', '<?= the_sub_field('event_name', 'options') ?>', 'submit');

				   		<? if( get_sub_field('redirect','option') ): ?>
				   			window.location.href = ('<?= the_sub_field('redirect_page', 'options') ?>');
				   		<? endif; ?>
			    	}

		    	<? endwhile; ?>
		    	}, false );
			</script>

		<? endif; ?>

	</head>

	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div class="button_container" id="toggle">
				Menu
		</div>

		<header id="header_bar">
			<div class="wrap">
				<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a>


					<nav class="desktop">

						<?php wp_nav_menu(array(
								'container' => false,
								'container_class' => 'menu cf',
								'menu' => __( 'The Main Menu', 'bonestheme' ),
								'menu_class' => 'nav top-nav cf',
								'theme_location' => 'main-nav',
						)); ?>

					</nav>

				</div>
			</header>



			<div id="mob_overlay">
				<div class="mob_content">
					<a class="logo" href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a>

					<nav id="nav_mobile">
						<?php wp_nav_menu(array(
								'container' => false,
								'container_class' => 'menu cf',
								'menu' => __( 'The Main Menu', 'bonestheme' ),
								'menu_class' => '',
								'theme_location' => 'main-nav',
						)); ?>
					</nav>

				</div>
			</div>
