	<div id="pdf_downloader">
	  <p><?php echo the_field('pdf_download_text','option');?></p>
		<a onClick="ga('send', 'event', { eventCategory: 'Brochure Download', eventAction: 'Click', eventLabel: 'Complete'});" class="pdf_btn" href="<?php echo the_field('pdf_download_file','option');?>" download>PDF Download</a>
		<div class="pdf_close_icon"><i class="fas fa-times"></i></div>
	</div>

	<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
		<div class="wrap">
			<p class="source-org copyright"><?php echo date('Y'); ?> &copy; <?php bloginfo( 'name' ); ?> | All rights Reserved | <a href="<?php echo home_url(); ?>/privacy-policy">Privacy Policy</a> | <a href="<?php echo home_url(); ?>/cookie-policy">Cookie Policy</a></p>
		</div>
	</footer>

</div>


<?php wp_footer(); ?>

<script>
document.addEventListener( 'wpcf7mailsent', function( event ) {
		ga('send', 'event', { eventCategory: 'Contact Form', eventAction: 'Submit', eventLabel: 'Complete'});
}, false );
</script>

</body>

</html> <!-- end of site. what a ride! -->
