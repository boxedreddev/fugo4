<?php get_header(); ?>

	<div class="upper-section">

		<? include 'components/masthead.php' ?>

		<? include 'components/overview.php' ?>

		<? include 'components/services.php' ?>

	</div>

	<? include 'components/steps.php' ?>

	<? include 'components/case_studies.php' ?>

	<? include 'components/our_model.php' ?>

	<? include 'components/contact_us.php' ?>

<?php get_footer(); ?>
