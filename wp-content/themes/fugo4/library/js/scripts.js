jQuery(document).ready(function($) {

      /* ====================== Init WOW ======================  */

        if ($(window).width() <= 520){
          $(".wow").removeClass("wow");
        }

        new WOW().init();


      // /* ====================== Menu Scroll Change ======================  *//

        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
            if (scroll >= 125) {
                $("#header_bar").addClass("collapse_inn");
                $("#toggle").addClass("top_pos_chng");
            } else {
                $("#header_bar").removeClass("collapse_inn");
                $("#toggle").removeClass("top_pos_chng");
            }
        });


        // /* ====================== Show PDF downloader ======================  *//

        $(window).scroll(function() {
          var scroll = $(window).scrollTop();
            if (scroll >= 150) {
                $("#pdf_downloader").addClass("pdf_vis");
            } else {
                $("#pdf_downloader").removeClass("pdf_vis");
            }
        });

        $('.pdf_close_icon').click(function() {
           $("#pdf_downloader").addClass('pdf_hider');
        });


        // /* ====================== Case Studies OWL ======================  *//

        $('.case_study_caro').owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          dots:false,
          rewind: true,
          animateOut: 'fadeOut',
          items:1
        });

        var owl = $(".case_study_caro");

        owl.owlCarousel();

        // Custom Navigation Events
        $(".next").click(function(){
          owl.trigger('next.owl.carousel');
          // console.log('next click works...');
        });
        $(".prev").click(function(){
          owl.trigger('prev.owl.carousel');
        });

        // /* ====================== Case Studies OWL ======================  *//



        // initiate owl different for counter...
        $(function(){
          var owlmod = $('.our_mod_caro');
            owlmod.owlCarousel({
              items:1,
              dots:true,
              rewind:true,
              // animateOut: 'fadeOut',
              onInitialized  : currentnum,
              onTranslated : currentnum
            });

            // Custom Navigation Events
            $(".nextmod").click(function(){
              owlmod.trigger('next.owl.carousel');
              // console.log('next click works...');
            });
            $(".prevmod").click(function(){
              owlmod.trigger('prev.owl.carousel');
            });

            function currentnum(event) {
               // var element   = event.target;         // DOM element, in this example .owl-carousel
               //  var items     = event.item.count;     // Number of items
                var item      = event.item.index + 1;     // Position of the current item
              $('.currentnum').html(item+".")
            }
          });



        // /* ======================  Hamburger ======================  *//
          $('#toggle').click(function() {
             $(this).toggleClass('active');
             $('#mob_overlay').toggleClass('hamburger_open');
             $('body').toggleClass('noscroller');
          });

          $('#nav_mobile li a').click(function() {

              $('#mob_overlay').removeClass('hamburger_open');

          });



    



        // /* ======================  ScrollMagic ======================  *//
          var controller = new ScrollMagic.Controller();

          // ---- Move Vehicle on Scroll ---- //
          // if($(window).width() > 768) {
          //     var animatesteplines = new ScrollMagic.Scene({
          //       triggerElement: ".line_hold",
          //       duration: '300',
          //       revers: 'false'
          //     })
          //
          //       .setClassToggle(".sep_line", "animate_line" )
          //
          //
          //       .addIndicators({name: "1 (add line animations...)"});
          //
          //     controller.addScene([
          //       animatesteplines
          //     ]);
          // }


});
