<div id="case_studies">
  <div id="case_lnk"></div>
  <div class="wrap">

        <div class="casestudies_slider_wrap wow fadeInUp" data-wow-delay="0.125s" data-wow-duration="0.75s">

          <div class="section_title">
            Case Studies
          </div>

          <div class="custom_cscontrol_wrap">
            <a class="next">Next <img src="<?php echo image('next_arrow.png');?>"></a>
            <a class="prev"><img src="<?php echo image('prev_arrow.png');?>"> Prev</a>
          </div>

          <div class="case_study_caro owl-carousel">
            <?php
            if( have_rows('case_studies') ):
                while ( have_rows('case_studies') ) : the_row();
                    $image = get_sub_field('image');
                ?>
                  <div class="item_wrapper">

                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="mobile_img" />

                    <div class="cs_content_wrap">
                      <h3><?php the_sub_field('cs_title');?></h3>
                      <?php the_sub_field('cs_textarea');?>
                    </div>

                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="desktop_img" />
                  </div>
                <?php
                endwhile;

            endif;
            ?>
          </div>

        </div>
        
    </div>

</div>
