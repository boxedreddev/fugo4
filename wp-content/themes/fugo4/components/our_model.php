<div id="our_modal">
  <div id="model_lnk"></div>
  <div class="title_area wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1s">

    <img src="<?php echo image('our_modal_bg.svg');?>" class="title_bg">
      <h3>Our <br> Model</h3>

      <div class="opening_content">
        <p><? the_field('our_modal_textarea') ?></p>
      </div>

  </div>

  <div class="modal_caro_wrap wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="1s">
    <div class="modal_slider_wrap">

      <div class="custom_modcontrol_wrap">
        <a class="nextmod">Next <img src="<?php echo image('next_arrow_alt.png');?>"></a>
        <a class="prevmod"><img src="<?php echo image('prev_arrow_alt.png');?>"> Prev</a>
      </div>

      <div class="our_mod_caro owl-carousel owl-theme">
        <?php
        if( have_rows('modal_items') ):
            while ( have_rows('modal_items') ) : the_row();
            ?>
              <div class="item">

                <div class="sidearea_left"></div>

                <div class="content_main">
                  <div class="content">
                    <div class="currentnum"></div>
                    <h5><?php echo the_sub_field('item_title');?></h5>
                    <?php echo the_sub_field('item_description');?>
                  </div>
                </div>

                <div class="sidearea_right"></div>

              </div>

            <?php
            endwhile;
        endif;
        ?>
      </div>

    </div>
</div>

</div>
