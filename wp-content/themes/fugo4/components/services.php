<div id="services">
	<div id="servicelnk"></div>
	<svg class="left_arrw" width="548px" height="397px" viewBox="0 0 548 397" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
	        <g id="Desktop-HD-Full" transform="translate(-106.000000, -1592.000000)">
	            <rect x="0" y="0" width="1440" height="10115"></rect>
	            <path d="M653,1988 L107,1593" id="Stroke-1" stroke="#67C3CC" stroke-width="2" stroke-linecap="round"></path>
	        </g>
	    </g>
	</svg>

	<div class="wrap">
		<img src="<?php echo image('our_services.png');?>" class="service_title">
	</div>

	<div class="wrap">
		<div class="col">
		<? $i = 0; ?>
		<? while ( have_rows('services') ) : the_row(); ?>

			<? if ($i == '3'): ?>
				</div>

				<div class="col">
			<? endif; ?>
			<div class="service wow fadeInUp" data-wow-delay="1s" data-wow-duration="1s">
				<div class="service-header">
					<div class="icon" style="background: url('<? the_sub_field('service_icon'); ?>') left center / contain no-repeat;"></div>
					<h3><? the_sub_field('service_title'); ?></h3>
				</div>

				<? the_sub_field('service_content'); ?>
			</div>

			<? $i++ ?>
		<? endwhile; ?>
		</div>

		<a href="#contact_lnk" class="btn teal">GET IN TOUCH FOR MORE INFORMATION</a>
	</div>
</div>
