<div class="inner_masthead">
	<div id="particles-js"></div>

	<div class="masthead-inner">
		<div class="wrap">
			<a href="#overview" class="scroll-arrow"><span>Scroll</span></a>

			<div class="title">

				<h1><?php echo the_title(); ?></h1>

			</div>

		</div>
	</div>
</div>
