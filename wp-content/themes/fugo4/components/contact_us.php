<div class="blue_bg">
  <div id="contact_lnk"></div>
  <section id="contact_us">
    <div id="particles-js2"></div>
      <div class="cfinfo-inner">
        <div class="wrap">

          <div class="contact_info">


          <img src="<?php echo image('contact_us.png');?>" class="contact_title">
            <p>
              <?php echo the_field('contact_us_paragraph','option');?>
            </p>

            <div class="contact_deets">
              Email: <a href="mailto:<?php echo the_field('email_address','option');?>" onClick="ga('send', 'event', { eventCategory: 'Email Link', eventAction: 'Click', eventLabel: 'Complete'});"><?php echo the_field('email_address','option');?></a><br>
              Telephone: <a href="tel:<?php echo the_field('phone_number','option');?>" onClick="ga('send', 'event', { eventCategory: 'Telephone Number', eventAction: 'Click', eventLabel: 'Complete'});"><?php echo the_field('phone_number','option');?></a>
            </div>

            <div class="social_icons">

              <?php if(get_field('twitter_url','option')) { ?>
                <div class="icon_wrap">
                  <a href="<?php echo the_field('twitter_url','option');?>" target="_blank">
                    <i class="fab fa-twitter"></i>
                  </a>
                </div>
              <?php } ?>

              <?php if(get_field('facebook_url','option')) { ?>
                <div class="icon_wrap">
                  <a href="<?php echo the_field('facebook_url','option');?>" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                  </a>
                </div>
              <?php } ?>

              <?php if(get_field('linkedin_url','option')) { ?>
                <div class="icon_wrap">
                  <a href="<?php echo the_field('linkedin_url','option');?>" target="_blank">
                    <i class="fab fa-linkedin-in"></i>
                  </a>
                </div>
              <?php } ?>

            </div>

        </div>

          <div class="contact_formm">
            <?php echo do_shortcode( '[contact-form-7 id="60" title="Contact Fugo4"]' ); ?>
          </div>



        </div>
      </div>
  </section>
</div>
