<?php get_header(); ?>


<div class="page_bg">

	<? include 'components/masthead_inner.php' ?>

</div>

			<div id="content">
				<div class="wrap">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


									<?php
										the_content();
									?>


							<?php endwhile; endif; ?>
						</div>

			</div>

<?php get_footer(); ?>
