<?php
# =========== GFORMS - HIDE LABELS =========== #
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

# =========== MOVE YOAST TO BOTTOM =========== #
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}
